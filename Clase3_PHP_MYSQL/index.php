<!DOCTYPE html>
<html lang="es">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="initial-scale=1, maximum-scale=1, width=device-width"><!--, user-scalable=no">-->

	<title>Clase 3</title>
	<meta name="description" content="App de registro de usuarios">
	<meta name="author" content="Pabhoz">

	<link rel="stylesheet" href="./stylesheets/stylesheet.css">

		<!--[if lt IE 9]>
			<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
			<![endif]-->

			<script src="./js/libs/jquery-1.11.1.js"></script>

		</head>
		<body>
			
			<form action="./registro.php" method="POST" enctype="multipart/form-data">
				<div class="title">Registro</div>
				<input type="text" name="uname" placeholder="Username" required>
				<input type="password" name="pass" placeholder="Password" required>
				<input type="text" name="email" placeholder="eMail" required>
				<input type="file" name="avatar" required>
				<input type="file" name="icono" required>
				<button>Registrar</button>
			</form>

			<script src="./js/script.js"></script>
		</body>
		</html>